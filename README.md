# The Cat Collective's Public Webpage

## Project Description
This repository contains the sources for The Cat Collective's webpage.

## Getting Started
Download the dependencies for the project and then clone the repo.
This project requires:
```
git
nodejs
npm
```

## Cloning the repo
Run the following command to clone the sources:
```bash
git clone https://gitlab.com/the-cat-collective/tcc-code/pages.git
```

## Viewing the Website From a Local Clone
Start the development server with `npm run dev` and then point your web browser at `http://localhost:4321/pages`.

## Licensing
* This project is licensed under the BSD 3-Clause license. Please refer to the [LICENSE](LICENSE) file for the full license text under which this repository is licensed.

## Disclaimer
None.