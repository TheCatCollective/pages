---
title: 'Feb-14 incident postmortem'
author: 'Astrid of The Cat Collective'
description: 'How to not be cyber-safe, brought to you by The Cat Collective'
pubDate: 2024-02-15
---
---
No intro today, let's get right to the meat of it. At 21:45 EST (UTC-5) on February 14th, 2024, we opened an unread message from one of our close friends on Steam and inadvertantly clicked a fake Steam gift card link during a lapse of judgement. This friend has been hacked earlier in the week but we had forgotten by last night. This lapse of judgement was caused due to us being partially proccuped with the touchpad on our main laptop bugging out momentarily. After
inspecting the link further, we realized that it was in-fact a scam link, but now had made our first mistake of leaving the tab with the phishing webpage opened. We then tabbed back to Discord and continued
the conversation we were having with one of our other friends. A few moments later, we did want to properly log into Steam as we were logged in from the web browser at the time. We then made another mistake when we then logged into the fake Steam page with the fake Steam page's phished email code as we were mid sentence and 
not paying much attention to what we're doing as this was an action we'd done a thousand times before, right? This made us panic and we immediately jumped into actual Steam and changed our password and enabled 2FA on our account via Steam Guard Mobile. We then changed the passwords for our Discord, Slack, Matrix, and many other platforms.  That leads us to current-time: 07:41 EST on February 15th, 2024, where we are now writing this document up.

## What are the lessons to be learned from this (hopefully minor) incident?
* Enable two-factor authentication on all services that support it.
* Use distinct passwords for all services and use a system such as a password manager to keep track of everything.
* Inspect all links for tampering before providing information to them.
* As much as possible, **pay attention** to what you are doing online.

Going forward, to mitigate the chance of further incidents, we will be setting up a password manager, randomizing all our passwords, and paying more attention when logging into services, even those that we are commonly used to logging in to.

We apologize for this lapse in judgement as it has gotten us in a small bit of hot water and has been quite the headache to clean out.