---
title: 'Uploading Private Project Configuration Files to Git is a Bad Idea'
author: 'ghost of The Cat Collective'
description: 'How to properly use API keys'
pubDate: 2024-01-28
---
---
Alright, it's about time SOMEONE here actually put this blog to some good use.
I'm ghost and I'll be your host for tonight's relatively cold take rant. Please do excuse any low quality writing, I do believe this is the first time any of us have actually attempted to write anything like this.

First of all, I'd like to start with an example many of you have probably heard of or probably have even done once before: You've spent a few hours on some new project and want to commit your work to git, and **BAM** you've also (hopefully accidentally) published the config file you use for the project with information such as API private keys and passwords. 

You've just compromised yourself and you might not even have noticed what had happened until either:
* A: Someone points it out
or
* B: You notice it in the tree later down the line

Now at this point you'd probably panic, go to change your keys and passwords, hoping some bot or malicious actor hasn't already gone through your repo, but sadly this might have already happened and there isn't really much you can do but change the logins on anything you've used  the same passwords for.

***But what if there were a better way? A way to prevent this from happening in the first place?***

Well, there are a few different ways, and I'll go over them in the rest of this post. 

The first method is to create a `gitignore` file.

To use one, just create a file named `.gitignore` within the root of your project directory (where your `.git` folder is) and add the local path to any files you want to exclude from the tree, however you might want to then also add a note to potential software users within the project's `README` that they need to now create this config by hand to get the code working properly.

There is an alternative way and that is to use environment variables to store sensitive information. Environmental variables are not exported to git when you push your repo so they stay on your system, and unless you make the variable persistent (google how to do this on your OS), the variables are wiped the next time you reboot your computer, so if you're concerned someone might grab them IRL, just reboot your PC after using the variables. With this method, you might have to leave a note in your file's `README.md` that users need to set environmental variables to use your software, but that should be considered a minor inconvenience at best for both the dev(s) and user(s).

I know that I did not go into depth about using anything like argument parsing or my personal favorite method of writing YAML or JSON configuration files with defaults, but that's a bit more complex and I might go into that some day, but that day is not today. If you want to figure out how to do that, read up on a guide on how to get access to command line arguments in your language of choice, as it's generally different based on language, and sometimes OS too.
