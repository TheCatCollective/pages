---
title: 'Some musings on mathematics'
description: 'Or how mathematics tends to be quite a rabbithole'
author: 'Astrid of The Cat Collective'
pubDate: 2023-12-24
---
---

A few months back, we were musing on finding solutions to the integral
$$ \displaystyle I\left(t\right) = \int t^{n}e^{kt} \,dt $$

Before we get started, we need to add some tools to our toolbox! Namely,
we need a special function known as the Gamma Function, which is defined by the following
recursive and integral definitions:

$$ 
\displaystyle \Gamma \left(n\right) = (n-1)! 
$$

and

$$ 
\displaystyle \Gamma \left(x\right) = \int_0^\infty {t^{x-1} e^{-t} \,dt} 
$$

Now that we have those, let's get started.
The original target integral has a form extremely similar to the gamma function (just without the bounds),
so our first thoughts make us think that this integral will have a solution in terms of
the gamma function.

This then brought two other functions to mind, those being the upper and lower incomplete gamma functions, which have the following forms respectively:

$$ 
\displaystyle \Gamma \left(s, x\right) = \int_x^\infty {t^{s-1} e^{-t} \,dt} 
$$

and

$$ 
\displaystyle \gamma \left(s, x\right) = \int_0^x {t^{s-1} e^{-t} \,dt} 
$$

Where $ \Gamma \left(s, x\right) $ is the upper incomplete gamma function and $ \gamma \left(s, x\right) $ is the lower incomplete gamma function.

By observing the bounds of $ \Gamma \left(s, x\right) $ and $ \gamma \left(s, x\right) $ we can derive a trivial identity for the complete gamma function of the form $ \gamma \left(s, x\right) + \Gamma \left(s, x\right) = \Gamma \left(s\right) $.

We first started out by doing repeated integration by parts on the target integral which led to the following
forms. We start with the n=1 case, which yields:

$$ 
\displaystyle I\left(1\right) = \frac{1}{k}te^{kt}-\frac{1}{k^{2}e^{kt}} + C 
$$

The next few forms are as follows:

$$ 
\displaystyle I\left(2\right) = \frac{t^{2}}{k}e^{kt}-\frac{2}{k^{2}}te^{kt}+\frac{2}{k^{3}}e^{kt}+C 
$$


$$ 
\displaystyle I\left(3\right) = \frac{t^{3}}{k}e^{kt}-\frac{3t^{2}}{k^{2}}e^{kt}+\frac{6}{k^{3}}te^{kt}-\frac{6}{k^{4}}e^{kt} + C 
$$


$$ 
\displaystyle I\left(4\right) = \left(\frac{(kt^{4})-{4\left(kt\right)}^{3}+12{\left(kt\right)}^{2}-24kt+24}{k^{5}}\right)e^{kt}+C 
$$


$$ 
\displaystyle \vdots 
$$


$$ 
\displaystyle I\left(t\right) = \int t^{n}e^{kt} \,dt = \frac{1}{k^{n+1}}\left(\sum_{m=0}^{n} \left(-1\right)^{m} \frac{n!}{\left(n-m\right)!}\left(kt\right)^{n-m}\right)e^{kt} + C 
$$

At one point I believe that we did attempt to find a closed form solution for this series but we were unable to after trying for a few days. If anyone does know how to find a proper closed form for this, please do not hesitate to contact us!